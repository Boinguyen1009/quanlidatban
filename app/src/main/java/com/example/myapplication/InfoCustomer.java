package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class InfoCustomer extends AppCompatActivity {
public TextView tvSoban;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info_customer);
        Button a = (Button)findViewById(R.id.btnNext);
        a.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(InfoCustomer.this, orderfood.class);
                startActivity(intent);
            }
        });

        tvSoban = (TextView)findViewById(R.id.soban);
        setDataByExtras();

    }
    public  void setDataByExtras(){
        Intent intent = getIntent();
        String datban = intent.getStringExtra(DatBan.DATBAN);
        tvSoban.setText(datban);
    }
}