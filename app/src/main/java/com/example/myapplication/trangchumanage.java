package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class trangchumanage extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trangchumanage);


        Button b6 = (Button) findViewById(R.id.btndatban1);
        b6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(trangchumanage.this, DatBan.class );
                startActivity(intent);
            }
        });

        Button a = (Button) findViewById(R.id.btnmanage);
        a.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(trangchumanage.this, EditFood.class);
                startActivity(intent);
            }
        });


    }
}