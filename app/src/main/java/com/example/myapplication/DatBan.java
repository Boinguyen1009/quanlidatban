package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

public class DatBan extends AppCompatActivity {
public  static final String DATBAN = "DATBAN";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dat_ban);

        ImageView b1 = (ImageView) findViewById(R.id.ban1);
        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String tvban = "1";
                byExtras(tvban);
            }
        });




        ImageView b2 = (ImageView) findViewById(R.id.ban2);
        b2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String tvban = "2";
                byExtras(tvban);
            }
        });
        ImageView b3 = (ImageView) findViewById(R.id.ban3);
        b3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String tvban = "3";
                byExtras(tvban);
            }
        });
        ImageView b4 = (ImageView) findViewById(R.id.ban4);
        b4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String tvban = "4";
                byExtras(tvban);
            }
        });
        ImageView b5 = (ImageView) findViewById(R.id.ban5);
        b5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String tvban = "5";
                byExtras(tvban);
            }
        });
        ImageView b6 = (ImageView) findViewById(R.id.ban6);
        b6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String tvban = "6";
                byExtras(tvban);
            }
        });
        ImageView b7= (ImageView) findViewById(R.id.ban7);
        b7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String tvban = "7";
                byExtras(tvban);
            }
        });
        ImageView b8 = (ImageView) findViewById(R.id.ban8);
        b8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String tvban = "8";
                byExtras(tvban);
            }
        });
        ImageView b9 = (ImageView) findViewById(R.id.ban9);
        b9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String tvban = "9";
                byExtras(tvban);
            }
        });
        ImageView b10 = (ImageView) findViewById(R.id.ban10);
        b10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String tvban = "10";
                byExtras(tvban);
            }
        });


    }
    public void byExtras(String datban){
        Intent intent = new Intent(DatBan.this, InfoCustomer.class);
        intent.putExtra(DATBAN,datban);
        startActivity(intent);
    }
}