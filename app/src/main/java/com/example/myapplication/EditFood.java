package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class EditFood extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_food);

        LinearLayout a = (LinearLayout) findViewById(R.id.edittable);
        a.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(EditFood.this, QLDatBan.class);
                startActivity(intent);
            }
        });

Button b = (Button) findViewById(R.id.them) ;
b.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        Intent intent = new Intent(EditFood.this, ThemEditFood.class);
        startActivity(intent);
    }
});

        Button c = (Button) findViewById(R.id.edit);
        c.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(EditFood.this, SuaEditFood.class);
                startActivity(intent);
            }
        });
    }
}