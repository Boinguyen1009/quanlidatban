package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class LogIn extends AppCompatActivity {

    Button Login;
    EditText Username, Pass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_in);
        //Chuyen form dang nhap
//        Button a = (Button)findViewById(R.id.btnLogin);
//        a.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent intent = new Intent(LogIn.this, trangchumanage.class);
//                startActivity(intent);
//            }
//        });


        EditText editText = findViewById(R.id.username);
        final TextView textViewusername = findViewById(R.id.usernameErorr);

        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length()==0){
                    textViewusername.setError("Không được để trống!");
                }else
                {
                    textViewusername.setError(null);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        Username = (EditText)findViewById(R.id.username);
        Pass = (EditText)findViewById(R.id.password);
        Login = (Button)findViewById(R.id.btnLogin);

        Login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String ID = "admin";
                String pass = "admin";
                if (Username.getText().toString().equals(ID) & Pass.getText().toString().equals(pass)){
                    Toast.makeText(getApplicationContext(),"Đăng nhập thành công!",Toast.LENGTH_LONG).show();
                    Intent h = new Intent(LogIn.this, trangchumanage.class);
                    startActivity(h);
                }
                else {
                    Toast.makeText(getApplicationContext(),"Đăng nhập thất bại, vui lòng kiểm tra lại user name hoặc password!", Toast.LENGTH_LONG).show();
                }
            }
        });

    }
}