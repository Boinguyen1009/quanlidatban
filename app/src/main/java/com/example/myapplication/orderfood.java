package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

public class orderfood extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_orderfood);

        ImageButton a = (ImageButton) findViewById(R.id.btndrink);
        a.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(orderfood.this, drink.class);
                startActivity(intent);
            }
        });
        ImageButton b = (ImageButton) findViewById(R.id.btndessert);
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(orderfood.this, dessert.class);
                startActivity(intent);
            }
        });
        ImageButton c = (ImageButton) findViewById(R.id.btnfood);
        c.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(orderfood.this, food.class);
                startActivity(intent);
            }
        });
    }
}