package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

public class QLDatBan extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_q_l_dat_ban);

        LinearLayout a = (LinearLayout) findViewById(R.id.editfood);
        a.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(QLDatBan.this, EditFood.class);
                startActivity(intent);
            }
        });

        Button b = (Button) findViewById(R.id.add);
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(QLDatBan.this, Themban.class);
                startActivity(intent);
            }
        });
        Button c = (Button) findViewById(R.id.edit);
        c.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(QLDatBan.this, suaban.class);
                startActivity(intent);
            }
        });
    }
}